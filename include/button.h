/*
 * button.h
 *
 * Created: 04-Jul-16 10:49:49 AM
 *  Author: Van Petrosyan
 */ 


#ifndef BUTTON_H_
#define BUTTON_H_

#define BUTTON_DIR			DDRB
#define BUTTON_PORT			PORTB
#define BUTTON_PIN			PINB
#define BUTTON_MINUS_PIN	PB6
#define BUTTON_PLUS_PIN		PB7

#define MIN_PRESS_TIMEOUT	5
#define DEBOUNCE_TIMEOUT	100
#define HELD_TIMEOUT		5000
#define TEST_TIMEOUT		10000

typedef enum __button__state__{BUTTON_IDLE, BUTTON_RELEASED, BUTTON_PRESSED, BUTTON_HELD}button_state;
	
/* init buttons */
void button_init();

/* handle buttons states */
void button_state_handler();

/* set minus button state */
void set_minus_button_state(button_state st);

/* set plus button state */
void set_plus_button_state(button_state st);

/* get minus button state */
button_state get_minus_button_state();

/* get plus button state */
button_state get_plus_button_state();

/* button test */
void button_test();

#endif /* BUTTON_H_ */