/*
 * control.h
 *
 * Created: 03.07.2016 21:52:06
 *  Author: Petrosyan Van
 */ 


#ifndef CONTROL_H_
#define CONTROL_H_

#define DEFAULT_SETPOINT_TEMPERATURE		25

#define MAX_SETPOINT_TEMP					70
#define MIN_SETPOINT_TEMP					10

#define TEMPERATURE_HISTERESYS				1
#define TEMPERATURE_CHANGE_ZONE				(TEMPERATURE_HISTERESYS + 1)

typedef enum __yecup__state__{YECUP_OFF, YECUP_ON}yecup_state;

/* application state handler */
void application_state_manager(void);

/* returns setpoint temperature */
int8_t get_setpoint_temperature();

/* set setpoint temperature */
void set_setpoint_temperature(int8_t temperature);

/* get yecup state */
yecup_state get_yecup_state();

/* set yecup state */
void set_yecup_state(yecup_state st);

/* turn off yecup */
void yecup_turn_off();

/* turn on yecup */
void yecup_turn_on();

#endif /* CONTROL_H_ */