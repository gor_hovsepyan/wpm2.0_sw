/*
 * battery.h
 *
 * Created: 03.07.2016 14:26:48
 *  Author: Petrosyan Van
 */ 


#ifndef POWER_H_
#define POWER_H_


#define BATT_ADC_CH							ADC_CH_ADC2
#define POWER_P								PINC

#define POWER_EN							PB1
#define CH_EN								PB2
#define PEN									PD4
#define WC_ST_PIN							PE1
#define CH-ST								PE2
#define W-HDD								PE3

	


#define BATT_WINDOW_SAMPLES					50			
#define TOP_RESISTOR						15000UL
#define BOTTOM_RESISTOR						10000UL
#define ADC_MAX								1023UL
#define REF_VOLTAGE							3.4F
#define MAX_VOLTAGE							8.4F
#define MIN_VOLTAGE							5.2F
#define ADC_MIN_VOLTAGE						(MIN_VOLTAGE * BOTTOM_RESISTOR * ADC_MAX) / ((TOP_RESISTOR + BOTTOM_RESISTOR) * REF_VOLTAGE)		
#define ADC_MAX_VOLTAGE						(MAX_VOLTAGE * BOTTOM_RESISTOR * ADC_MAX) / ((TOP_RESISTOR + BOTTOM_RESISTOR) * REF_VOLTAGE)
#define BATTERY_ERROR						-1		
#define BATTERY_LOW_TRESHOLD				5						
#define INPUT_PORT_C						PINC
#define INPUT_PORT_DD						PIND
#define INPUT_PORT_D						PORTD


typedef enum __pwr__state__{ACTIVE, PASSIVE }pwr_state;
void power_init();
pwr_state Get_Charger_State();
pwr_state Get_Power_State();  
pwr_state Get_ChargerIC_State();
uint16_t Calculate_battery_level(uint16_t);
uint16_t battery_state_handler();
uint16_t get_battery_state();
void Enable_Power();
void Disable_Power();
void Enable_ChargerIC();
void Disable_ChargerIC();
pwr_state Get_WC_State();
#endif 