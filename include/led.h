/*
 * led.h
 *
 * Created: 13-Nov-17 4:58:43 PM
 *  Author: gorho
 */ 


#ifndef LED_H_
#define LED_H_

#define LED_RED_PIN			 PD3
#define LED_GREEN_PIN		 PD5
#define LED_BLUE_PIN		 PD6

#define FADE_PERIOD			50
#define FADE_TIME		(1000000UL / FADE_PERIOD) / FADE_PERIOD
typedef enum __led__color__{RED, GREEN, BLUE }led_color;

/* LED initialization*/
void led_init();
void Led_Off(led_color color);
void Led_On(led_color color);
void Led_Blink(led_color color);
void Dual_Blink(led_color color_1, led_color color_2);
#endif /* LED_H_ */