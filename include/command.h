/*
 * command.h
 *
 * Created: 02-Dec-17 4:23:48 PM
 *  Author: gorho
 */ 


#ifndef COMMAND_H_
#define COMMAND_H_


#define GET_BATTERY_STATE				"WC+GET+BATTERY+STATE"
#define GET_CHARGER_STATE				"WC+GET+CHARGER+STATE"
#define GET_CHARGERIC_STATE				"WC+GET+CHRGIC+STATE"
#define SET_CHARGERIC_ON				"WC+SET+CHRGIC+ON"
#define SET_CHARGERIC_OFF				"WC+SET+CHRGIC+OFF"
#define GET_POWER_STATE					"WC+GET+PWR+STATE"
#define SET_POWER_ON					"WC+SET+PWR+ON"
#define SET_POWER_OFF					"WC+SET+PWR+OFF"
#define SET_COLOR					    "WC+SET_COLOR"
#define SET_TIME						"WC+TIME+"
#define SET_ALARM						"WC+ALRM+"
#define SEND_IP							"WC+SEND+IP+"
#define SEND_SSID						"WC+SEND+SSID+"
#define SEND_WIFI_Status				"WC+SEND+WiFi+"
#define SEND_VPN_Status					"WC+SEND+VPN+"
int n;
extern char* ma;
extern char* ipAddress;
extern char ip_data[18];
/*
extern char* *a; 
extern char* ssid[10];
extern char* vpn[16];
extern char* ssid[6];
extern char* wifi[6];*/


typedef enum __command__{
	NO_COMMAND,
	GET_CHARGER_STATE_COMMAND,
	GET_CHARGERIC_STATE_COMMAND,
	SET_CHARGERIC_ON_COMMAND,
	SET_CHARGERIC_OFF_COMMAND,
	GET_BATTERY_STATE_COMMAND,
	GET_POWER_STATE_COMMAND,
	SET_POWER_ON_COMMAND,
	SET_POWER_OFF_COMMAND,
	SET_TIME_COMMAND,
	SET_ALARM_COMMAND,
	SET_DATE_COMMAND,
	SEND_IP_COMMAND,
} command;

void copy_command ();
command command_parser();
void command_handler();
void Send_Power_State();
void Send_Chargeric_State();
void Send_Charger_State();
void Send_Battery_State();
void Set_RTC_Time();
void Get_RTC_Time();
void Set_Alarm();
void SET_IP_Address();
char* GET_IP_Address(char*);
char* VPN_Status(char*);
char* WiFi_Status(char*);
char* WiFi_SSID(char*);

#endif /* COMMAND_H_ */