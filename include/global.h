/*
 * global.h
 *
 * Created: 10/20/2015 12:48:06 PM
 *  Author: ASTRO4
 */ 


#ifndef GLOBAL_H_
#define GLOBAL_H_


#include "avrlibdefs.h"
#include "avrlibtypes.h" // global AVRLIB types definitions

#define USART_BAUDRATE 38400


// CPU clock speed
  #define F_CPU        16000000               		// 16MHz processor
//#define F_CPU        14745000               		// 14.745MHz processor
//#define F_CPU        000000               		// 8MHz processor
//#define F_CPU        7372800               		// 7.37MHz processor
//#define F_CPU        4000000               		// 4MHz processor
//#define F_CPU        3686400               		// 3.69MHz processor
#define CYCLES_PER_US ((F_CPU+500000)/1000000) 	// cpu cycles per microsecond
#define __PROG_TYPES_COMPAT__


#endif /* GLOBAL_H_ */