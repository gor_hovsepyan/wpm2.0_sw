/*
 * System.h
 *
 * Created: 30-Nov-17 4:05:55 MP
 *  Author: gorho
 */ 


#ifndef SYSTEM_H_
#define SYSTEM_H_

#define Button1_Pin			PD2
#define Button2_Pin			PD7
#define Button_Port			PORTD
#define Button_Input		PIND	
#define MIN_PRESS_TIMEOUT	5
#define DEBOUNCE_TIMEOUT	100
#define HELD_TIMEOUT		5000
#define TEST_TIMEOUT		10000

#define WC_BT				PB0
typedef enum __button__state__
{
	BUTTON_IDLE,
	BUTTON_RELEASED,
    BUTTON_PRESSED,
    BUTTON_HELD
} button_state;



void set_Button_state(button_state);
void button_state_handler();
button_state get_Button_state();
void Button1_state();
inline void Button2_state();
void system_handler();
void Press_WC_Button();
void Long_Press_WC_Button();

#endif /* SYSTEM_H_ */