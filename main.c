/*
 * 
 *
 * Created: 7/2/2016 5:03:37 PM
 * Author : Hovsepyan Gor
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include "global.h"
#include <util/delay.h>
#include <util/atomic.h>
#include <string.h>
#include "drivers/a2d.h"
#include "drivers/uart.h"
#include "PCF8563.h"
#include "power.h"
#include "led.h"
#include "string.h"
#include "command.h"
#include "system.h"
#include <avr/io.h>
#include "SSD1306.h"

uint16_t count =1;
 BOOL power_event= FALSE;

const int x=0;
const int y=2;

/*
volatile uint16_t total_overflow;

void timer0_init(void)
{
	// Setting the 1024 prescaler
	TCCR0B |= (1 << CS00) | (1 << CS02);

	// Initialize Timer0
	TCNT0 = 0;

	// Initialize the overflow interrupt for TIMER0
	TIMSK0 |= (1 << TOIE0);

	// Enable global interrupts
	sei();

	// Initialize total_overflow variable
	total_overflow =0;
}
ISR(TIMER0_OVF_vect)
{
	total_overflow++;
}*/



    // send buffer to display
int main(void)
{

a2dInit(); //ADC initialization 
power_init();
led_init();
uartInit(9600);
PCF_Init(PCF_ALARM_INTERRUPT_ENABLE);	
PCF_SetTimer(PCF_TIMER_4096HZ, 100);
uint8_t ac[1];
uint32_t state =0;
uint8_t longstate =0;
uint32_t counter=0;
uint16_t bb[1];
uint32_t time;
uint16_t bu[1];
uint8_t buu[1];


	Led_Off(GREEN);
	Led_Off(BLUE);
	Led_Off(RED);
	
 	GLCD_Setup();
	GLCD_InvertScreen();


int i =0;
char *ae;



    while (1) 
    {
		button_state_handler();
		command_handler();
		i++;
		_delay_ms(10);
	}

}