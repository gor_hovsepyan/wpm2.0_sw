/*
 * led.c
 *
 * Created: 13-Nov-17 4:59:39 PM
 *  Author: gorho
 */ 
#include <avr/io.h>
#include "global.h"
#include <util/delay.h>
#include "led.h"

/* LED initialization*/
void led_init()
{
	DDRD |= (1<<DDRD6);
	DDRD |= (1<<DDRD3);
	DDRD |= (1<<DDRD5);
}


void Led_Off(led_color color)
{
		if (color == RED )
		{
			PORTD |= (1 << LED_RED_PIN);
		}
		else if (color == GREEN)
		{
			PORTD |= (1 << LED_GREEN_PIN);
		}
		else if(color == BLUE)
		{
			PORTD |= (1 << LED_BLUE_PIN);
		}
}
void Led_On(led_color color)
{
	if (color == RED )
	{
		PORTD &= ~(1 << LED_RED_PIN);
	}
	else if (color == GREEN)
	{
		PORTD &= ~(1 << LED_GREEN_PIN);
	}
	else if(color == BLUE)
	{
		PORTD &= ~(1 << LED_BLUE_PIN);
	}
}

void Led_Blink(led_color color)
{
		if (color == RED )
		{
			Led_On(RED);
			_delay_ms(300);
			Led_Off(RED);
			_delay_ms(300);
		}
		else if (color == GREEN)
		{
			Led_On(GREEN);
			_delay_ms(300);
			Led_Off(GREEN);
			_delay_ms(300);
		}
		else if(color == BLUE)
		{
			Led_On(BLUE);
			_delay_ms(300);
			Led_Off(BLUE);
			_delay_ms(300);
		}
}
Dual_Blink(led_color color_1, led_color color_2)
{
	Led_On(color_1);
	Led_On(color_2);
	_delay_ms(300);
	Led_Off(color_1);
	Led_Off(color_2);
	_delay_ms(300);
}