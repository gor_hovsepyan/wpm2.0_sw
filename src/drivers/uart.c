/*! \file uart.c \brief UART driver with buffer support. */
// *****************************************************************************
//
// File Name	: 'uart.c'
// Title		: UART driver with buffer support
// Author		: Pascal Stang - Copyright (C) 2000-2002
// Created		: 11/22/2000
// Revised		: 06/09/2003
// Version		: 1.3
// Target MCU	: ATMEL AVR Series
// Editor Tabs	: 4
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//
// *****************************************************************************

#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include "global.h"
#include "drivers/uart.h"

// UART global variables
// flag variables
volatile unsigned char data_in[COMMAND_MAX_SIZE];
volatile unsigned char data_count;
volatile unsigned char command_ready = FALSE;


// enable and initialize the uart
void uartInit(u32 baudrate)
{
	// Turn on USART hardware (RX, TX)
	UCSR0B |= (1 << RXEN0) | (1 << TXEN0);
	// 8 bit char sizes
	UCSR0C |= (1 << UCSZ00) | (1 << UCSZ01);
	// set double speed
	#ifdef U2X_SET
	UCSR0A |= (1 << U2X0);
	#endif
	// set baud rate
	uartSetBaudRate(baudrate);  
	// Enable the USART Receive interrupt
	UCSR0B |= (1 << RXCIE0 );
	// enable interrupts
	sei();
}



// set the uart baud rate
void uartSetBaudRate(u32 baudrate)
{
	// calculate division factor for requested baud rate, and set it
	u16 bauddiv = ((F_CPU+(baudrate*8L))/(baudrate*16L)-1);
	#ifdef U2X_SET	
	bauddiv = ((F_CPU+(baudrate*4L))/(baudrate*8L)-1);
	#endif
	outb(UBRR0L, bauddiv);
	#ifdef UBRRH
	outb(UBRR0H, bauddiv>>8);
	#endif
}

void uartSendByte(char send)
{
	// Do nothing for a bit if there is already
	// data waiting in the hardware to be sent
	while ((UCSR0A & (1 << UDRE0)) == 0) {};
	UDR0 = send;
}

void uartSendBuffer (const char *send)
{
	// Cycle through each character individually
	while (*send) {
		uartSendByte(*send++);
	}
}




// UART Receive Complete Interrupt Handler
// USE "USART_RX_vect" for Atmega 168PB

ISR (USART0_RX_vect) /**/
{
	if(command_ready == TRUE)
		return;
	if(data_count == COMMAND_MAX_SIZE)
	{
		command_ready = FALSE;
		data_count = 0;
		clear_data_buffer();
		return;
	}
	// Get data from the USART in register
	data_in[data_count] = UDR0;	
	if (data_in[data_count] == '\0' || data_in[data_count] == '\r' || data_in[data_count] == '\n' || data_in[data_count] == 'Z') {
		command_ready = TRUE;
		// Reset to 0, ready to go again
		data_count = 0;
		} else {
		data_count++;
	} 
}


u08 get_command_ready()
{
	return command_ready;
}

void set_command_ready(u08 st)
{
	command_ready = st;
}


void clear_data_buffer()
{
	for(int i = 0; i < COMMAND_MAX_SIZE; i++)
		data_in[i] = 0;
	data_count = 0;	
}