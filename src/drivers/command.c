/*
 * command.c
 *
 * Created: 02-Dec-17 4:24:18 PM
 *  Author: gorho
 */ 
#include "command.h"
#include <avr/io.h>
#include <stdlib.h>
#include <util/atomic.h>
#include <util/eu_dst.h>
#include <string.h>
#include "global.h"
#include <util/delay.h>
#include "power.h"
#include "drivers/uart.h"
#include <time.h>
#include "PCF8563.h"


char command_in[COMMAND_MAX_SIZE];
PCF_DateTime getTime;
uint16_t date;
uint32_t hours;
uint32_t minutes;
uint32_t seconds;
struct tm tm;
char buf[255];
const time_t timeshift=946684800;
time_t  unix_time;
time_t  unix_alarm;
char* ipAddress;
struct tm * timeinfo;
struct tm * alarminfo;
extern time_t  unix_time; 
extern time_t  unix_alarm; 
char* ma ="192.168.44.0";
char ip_data[18] = "";
/*
char* ssid[10] = "";
char* vpn[16] = "";
char* ssid[6] = "";
char* wifi[6] = "";*/

void copy_command ()
{
	// The USART might interrupt this - don't let that happen!
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		// Copy the contents of data_in into command_in
		for(int i = 0; i < COMMAND_MAX_SIZE; i++)
		command_in[i] = data_in[i];
		clear_data_buffer();
	}
}


command command_parser()
{
	if (get_command_ready() == FALSE)
	return NO_COMMAND;
	copy_command();
	set_command_ready(FALSE);
	if(strstr(command_in, GET_BATTERY_STATE) != NULL)
	return GET_BATTERY_STATE_COMMAND;
	else if(strstr(command_in, GET_CHARGER_STATE) != NULL)
	return GET_CHARGER_STATE_COMMAND;
	else if(strstr(command_in, GET_CHARGERIC_STATE) != NULL)
	return GET_CHARGERIC_STATE_COMMAND;
	else if(strstr(command_in, SET_CHARGERIC_ON) != NULL)
	return SET_CHARGERIC_ON_COMMAND;
	else if(strstr(command_in, SET_CHARGERIC_OFF) != NULL)
	return SET_CHARGERIC_OFF_COMMAND;
	else if(strstr(command_in, SET_POWER_ON) != NULL)
	return SET_POWER_ON_COMMAND;
	else if(strstr(command_in, SET_POWER_OFF) != NULL)
	return SET_POWER_OFF_COMMAND;
	else if(strstr(command_in, GET_POWER_STATE) != NULL)
	return GET_POWER_STATE_COMMAND;
	else if(strstr(command_in, SET_COLOR) != NULL)
	return GET_BATTERY_STATE_COMMAND;
	else if(strstr(command_in, SET_TIME) != NULL)
	{
			
		long time_buffer[1];
		char time_data[11] = "";
		strncpy(time_data, command_in + strlen(SET_TIME), 14);
		time_data[14] = '\0';
	    unix_time  = atol(time_data);
		return SET_TIME_COMMAND;
	} 
	else if(strstr(command_in, SET_ALARM) != NULL)
	{
	
		long alarm_buffer[1];
		char alarm_data[11] = "";
		strncpy(alarm_data, command_in + strlen(SET_ALARM), 11);
		alarm_data[11] = '\0';
		unix_alarm  = atol(alarm_data);
		return SET_ALARM_COMMAND;
	}
		else if(strstr(command_in, SEND_IP) != NULL)
	{
		
		long ip_buffer[1];
		strncpy(ip_data, command_in +strlen(SEND_IP), strlen(command_in)-strlen(SEND_IP)-1);
		
		ipAddress=ip_data;
		
		return SEND_IP_COMMAND;
		
	}
		/*else if(strstr(command_in, SEND_IP) != NULL)
		{
			
			long ip_buffer[1];
			
			strncpy(ip_data, command_in + strlen(SEND_SSID), 15);
			ipAddress=ip_data;
			return SEND_IP_COMMAND;
		}	*/
	
	
	
}



void Send_Power_State()
{
	if(Get_Power_State()==ACTIVE)
		{
			uartSendBuffer("PWR_ST_ACTIVE");
		}
		else
			uartSendBuffer("PWR_ST_PASSIVE");
}
void Send_Chargeric_State()
{
	if(Get_ChargerIC_State()==ACTIVE)
	{
		uartSendBuffer("ChrgIC_ST_ACTIVE");
	}
	else
	uartSendBuffer("ChrgIC_PASSIVE");
}
void Send_Charger_State()
{
	if(Get_Charger_State()==ACTIVE)
	{
		uartSendBuffer("Chrg_ST_ACTIVE");
		uartSendByte('\n');
	}
	else
	uartSendBuffer("Chrg_PASSIVE");
}

void Send_Battery_State()
{
	char* buff[6];
	itoa(get_battery_state(),buff,10);
	uartSendBuffer(buff); ///////
	uartSendByte('\n');
}

void  Set_RTC_Time()
{
	PCF_Init(PCF_ALARM_INTERRUPT_ENABLE | PCF_TIMER_INTERRUPT_ENABLE);
	
	time_t a[2];
	time_t shifted_unix =unix_time-timeshift;
    timeinfo = localtime(&shifted_unix);
	static PCF_DateTime dateTime;
	dateTime.second = timeinfo->tm_sec;
	dateTime.minute = timeinfo->tm_min;
	dateTime.hour = timeinfo->tm_hour;
	dateTime.day = timeinfo->tm_mday;
	dateTime.weekday = timeinfo->tm_wday;
	dateTime.year = 1900+timeinfo->tm_year;
	PCF_SetDateTime(&dateTime);
}

void Get_RTC_Time()
{
	long bu[1];
				PCF_GetDateTime(&getTime);
				ltoa(getTime.second,bu,10);
				uartSendBuffer(bu);	
				uartSendByte('\n');
}

void Set_Alarm()
{
	time_t shifted_alarm =unix_alarm-timeshift;
	
		alarminfo=(localtime(&shifted_alarm));
		PCF_GetDateTime(&getTime);	

	if(getTime.minute == alarminfo->tm_min && getTime.hour == alarminfo->tm_hour)
	{
		DDRD |= (1<<DDD6);
		PORTD &= ~(1 << PD6);
	}
}

char* IP_Address()
{
// 	ma=ipAddress;
// 	uartSendBuffer(ma);
//	uartSendBuffer(ipAddress);
	return ipAddress;
}
char* VPN_Status(char* vpn)
{
	return vpn;
}
char* WiFi_Status(char* wifi)
{
	return wifi;
}
char* WiFi_SSID(char* ssid)
{
	return ssid;
}
void SET_IP_Address()
{
	
}
char* GET_IP_Address(char* e)
{
	uartSendBuffer(e);
	return (e);
}
void command_handler()
{
	command WC_command = command_parser();
	switch (WC_command)
	{ 
		case SET_POWER_OFF_COMMAND:
			Disable_Power();
			break;
		case SET_POWER_ON_COMMAND:
			Enable_Power();	
			break;
		case SET_CHARGERIC_OFF_COMMAND:
			Disable_ChargerIC();
			break;
		case SET_CHARGERIC_ON_COMMAND:
			Enable_ChargerIC();
			break;
		case GET_BATTERY_STATE_COMMAND:
			Send_Battery_State();
			break;
		case GET_CHARGERIC_STATE_COMMAND:
			Send_Chargeric_State();
			break;
		case GET_CHARGER_STATE_COMMAND:
			Send_Charger_State();
			break;
		case GET_POWER_STATE_COMMAND:
			Send_Power_State();
			break;
		case SET_TIME_COMMAND:
			Set_RTC_Time();
			break;
		case SET_ALARM_COMMAND:
			Set_Alarm();
			break;
		case SEND_IP_COMMAND:
			SET_IP_Address();
			break;
	}
}