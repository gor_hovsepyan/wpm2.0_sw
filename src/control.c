/*
 * control.c
 *
 * Created: 03.07.2016 21:51:44
 *  Author: Hovsepyan Gor
 */ 

#include <avr/io.h>
#include "global.h"
#include <util/delay.h>
#include "ble.h"
#include "led.h"
#include "power.h"
#include "button.h"
#include "control.h"

int8_t setpointTemperature = DEFAULT_SETPOINT_TEMPERATURE;
yecup_state yecupState = YECUP_OFF;

/* returns setpoint temperature */
int8_t get_setpoint_temperature()
{
	return setpointTemperature;
}

/* set setpoint temperature */
void set_setpoint_temperature(int8_t temperature)
{
	if(temperature < MIN_SETPOINT_TEMP)
		temperature = MIN_SETPOINT_TEMP;
	else if(temperature > MAX_SETPOINT_TEMP)
		temperature = MAX_SETPOINT_TEMP;
	setpointTemperature = temperature;
}

/* turn off yecup */
void yecup_turn_off()
{
	if (get_yecup_state() == YECUP_ON)
	{
		set_led_state(OFF);
		set_peltier_state(PELTIER_OFF);
		set_fan_state(FAN_OFF);
		set_yecup_state(YECUP_OFF);
	}	
}

/* turn on yecup */
void yecup_turn_on()
{
	if(get_yecup_state() == YECUP_OFF)
	{
		if(get_battery_state() <= BATTERY_LOW_TRESHOLD)
		return;
		set_yecup_state(YECUP_ON);
		set_led_state(GREEN);
	}		
}



/* application state handler */
void application_state_manager()
{
	/* battery fsm */
/*	if(get_battery_state() <= BATTERY_LOW_TRESHOLD)
	{
		yecup_turn_off();
		return;
	} */
	/* button fsm */
	if((get_minus_button_state() == BUTTON_HELD) || (get_plus_button_state() == BUTTON_HELD))
	{
		yecup_turn_off();
		return;
	}
	if(get_minus_button_state() == BUTTON_PRESSED && get_plus_button_state() == BUTTON_IDLE)
	{
		set_setpoint_temperature(MIN_SETPOINT_TEMP);
		yecup_turn_on();
	}
	if(get_plus_button_state() == BUTTON_PRESSED && get_minus_button_state() == BUTTON_IDLE)
	{
		set_setpoint_temperature(MAX_SETPOINT_TEMP);
		yecup_turn_on();
	}
	/* charger fsm */
	if(((get_charger_state() == CHARGER_ON) || (get_charger_state() == CHARGER_USB)) && get_yecup_state() == YECUP_OFF)
		set_led_state(GREEN);
	else if(((get_charger_state() == CHARGER_OFF) || (get_charger_state() == CHARGER_FAULT)) && get_yecup_state() == YECUP_OFF)
		set_led_state(OFF);
		
	if(get_water_temperature() == NTCTEMP_LOOKUPRETERROR)
	{
		yecup_turn_off();
		return;
	}

	/* temperature fsm */
	if(get_yecup_state() == YECUP_ON)
	{
		if ((int8_t)get_water_temperature() < (get_setpoint_temperature() - TEMPERATURE_CHANGE_ZONE))
		{
			set_fan_state(FAN_OFF);
			set_peltier_state(PELTIER_HEATING);
			set_led_state(RED);			
		}
		else if((int8_t)get_water_temperature() > (get_setpoint_temperature() + TEMPERATURE_CHANGE_ZONE))
		{
			set_fan_state(FAN_ON);
			set_peltier_state(PELTIER_COOLING);
			set_led_state(BLUE);
		}
		else if(((int8_t)get_water_temperature() > (get_setpoint_temperature() - TEMPERATURE_HISTERESYS)) && ((int8_t)get_water_temperature() < (get_setpoint_temperature() + TEMPERATURE_HISTERESYS)))
		{
			set_led_state(GREEN);
			set_peltier_state(PELTIER_OFF);
			set_fan_state(FAN_OFF);
		}		
	}	
}

/* get yecup state */
yecup_state get_yecup_state()
{
	return yecupState;
}

/* set yecup state */
void set_yecup_state(yecup_state st)
{
	yecupState = st;
}