/*
 * system.c
 *
 * Created: 30-Nov-17 4:06:34 PM
 *  Author: gorho
 */ 
#include "system.h"
#include <avr/io.h>
#include "global.h"
#include <util/delay.h>
#include "drivers/a2d.h"
#include "power.h"
#include "stdbool.h"
#include "led.h"
#include "SSD1306.h"
#include "SSD1306_Settings.h"
#include "PCF8563.h"
#include "command.h"

PCF_DateTime gTime;
static button_state Button_state = BUTTON_IDLE;
uint16_t timer = 0;
uint8_t press_count =0;
uint8_t button2_counter = 0;
extern uint8_t bt2=0;
char* *c;

int min;
void Button1_state()
{
	static bool buttonActive = false;
	static bool longPressActive = false;
	static uint16_t counter = 0;
	uint8_t btn_state = 0;
	
	   while(!is_high(Button_Input, Button2_Pin))
	   {
		   buttonActive=true;
		   timer++;
		   
		   _delay_ms(1);
			     if(timer>= 500)
			     {
				     timer=0;
					 press_count++;
					 longPressActive=true;
					 Led_On(GREEN);
			     } 
	   }
	   
	    if (timer>=10 && timer<=450 && is_high(Button_Input, Button1_Pin))
	    {
			if (press_count==0)
			{
				Led_On(RED);	
			}
			
			timer=0;
			press_count=0;
	    }
		
	   }
	   
void Button2_state()
{
	while(!is_high(Button_Input, Button1_Pin) )
	{
		_delay_ms(5);
			if(is_high(Button_Input, Button1_Pin))
			{
				button2_counter++;
				if(button2_counter%4==0)
				{
					bt2=1;
				}
				if(button2_counter%4==1)
				{
					bt2=2;
				}
				if(button2_counter%4==2)
				{
					bt2=3;
				}
				}
				if(button2_counter%4==3)
				{
					bt2=4;
				}
			}
	}
	



	   
void set_Button_State(button_state st)
{
	Button_state=st;
}

button_state get_Button_State()
{
	return Button_state;
} 

void Press_WC_Button()
{	
			PORTE &= ~(1 <<WC_BT);
			_delay_ms(1000);
			PORTE |= (1 << WC_BT);
			uartSendBuffer("WC_POWER_OFF");
}

void Long_Press_WC_Button()
{
		PORTE &= ~(1 <<WC_BT);
		_delay_ms(5000);
		PORTE |= (1 << WC_BT);
		uartSendBuffer("WC_LOMG_POWER_OFF");
}

void button_state_handler()
{
	Button1_state();
	Button2_state();
	button_state BtnState = get_Button_State();
	if(BtnState==BUTTON_HELD/* && Get_WC_State()==ACTIVE*/)
	{
		return 1;
		//Disable_Power();
	}
	if(BtnState==BUTTON_PRESSED && Get_WC_State()==ACTIVE)
	{
		return 3;
		/*Enable_Power();
		Press_WC_Button(); */
	}
	if(BtnState=BUTTON_IDLE)
	{
		return 0;
	}
	
	if(bt2==1)
	{
			c=&ipAddress;
		    Display_Network_Status(ip_data, "OF", "On", "ON");
			uartSendBuffer(ip_data);
	}
	if(bt2==2)
	{
		GLCD_Clear();
		Display_Battery_Status(Calculate_battery_level(a2dConvert10bit(BATT_ADC_CH)));	
	}
	if(bt2==3)s
	{
			
	}
	if(bt2==4)
	{
		Display_DateTime();
	}
/*
	if(BtnState==BUTTON_PRESSED && Get_WC_State()==ACTIVE)
	{ 
		return 2;
		Press_WC_Button();
	} */
}



void system_handler()
{
	
	if (get_battery_state()<=10 ) 
	{
		if(Get_WC_State()==ACTIVE)
		{
			uartSendBuffer("WC_POWER_OFF");
			uartSendByte('\n');
			uartSendByte('\r');
			_delay_ms(200);	
		}
		else
			Disable_Power();
	}
	
	if(Get_WC_State()==PASSIVE)
	{
		Disable_Power();
	}
	
}


