/*
 * ntc_temperature.c
 *
 * Created: 7/2/2016 5:40:26 PM
 *  Author: Van Petrosyan
 */ 

#include <avr/io.h>
#include <avr/pgmspace.h>
#include "global.h"
#include "drivers/a2d.h"
#include "ntc_temperature.h"

/* Temperature table for  NTCALUG03A103 thermistor */
static const float Table[] PROGMEM  = {	334.27, 241.32, 176.13, 129.90, 96.761, 72.765, 55.218, 42.268, 32.624, 25.381, 
										19.897, 15.711, 12.493, 10.000, 8.056, 6.530, 5.324, 4.365, 3.599, 2.982, 2.484, 
										2.079, 1.748, 1.476, 1.252, 1.066, 0.912, 0.782, 0.674, 0.583, 0.506, 0.440, 0.384, 0.337};

int16_t waterTemperature;

#define TEMPERATURE_TABLE_READ(i) pgm_read_float(&Table[i])


/* returns temperature value*/
float calculate_temperature(uint16_t adc_value)
{
	uint8_t i = 0;
	if (adc_value == 0)
		return NTCTEMP_LOOKUPRETERROR;
	float resistance = ((RES_VALUE * (float)(ADC_MAX - adc_value)) / adc_value) / 1000; // get resistance value 
	float t = 0.0;
	float mint = 0;
	float maxt = 0;

	//return error for invalid adc values
	if((resistance < TEMPERATURE_TABLE_READ(NTCTEMP_LOOKUPTABLE_SIZE - 1)) || ((resistance > TEMPERATURE_TABLE_READ(0)))) 
		return NTCTEMP_LOOKUPRETERROR;

	
	for(i = 0; i < NTCTEMP_LOOKUPTABLE_SIZE; i++) 
	{
		if(resistance > TEMPERATURE_TABLE_READ(i))
			break;
	}
	mint = TEMPERATURE_TABLE_READ(i);
	maxt = TEMPERATURE_TABLE_READ(i - 1);
	
	t = (MIN_NTC_TEMP + TEMPERATURE_TABLE_STEP * i) - (TEMPERATURE_TABLE_STEP * (resistance - mint)) / (maxt - mint); //do interpolation
	return t;
}


/* Main function for temperature reading */
void ntc_temperature_handler()
{
	static uint16_t adc_sum_value = 0;
	static uint16_t window_counter = 0; 
	uint16_t adc_value;
	adc_value = a2dConvert10bit(TEMP_ADC_CH);
	adc_sum_value += adc_value;
	if(++window_counter == TEMP_WINDOW_SAMPLES)	
	{
		adc_value = adc_sum_value / window_counter;
		waterTemperature = (int16_t)calculate_temperature(adc_value);
		window_counter = adc_sum_value = 0;		
	}
}

/* returns temperature value*/
int16_t get_water_temperature()
{
	return waterTemperature;
}