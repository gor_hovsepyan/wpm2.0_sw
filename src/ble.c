/*
 * ble.c
 *
 * Created: 03.07.2016 11:53:35
 *  Author: Hovsepyan Gor
 */ 

#include <avr/io.h>
#include <stdlib.h>
#include <util/atomic.h>
#include <string.h>
#include "global.h"
#include <util/delay.h>
#include "drivers/uart.h"
#include "power.h"
#include "control.h"
#include "button.h"
#include "led.h"
#include "ble.h"



char command_in[COMMAND_MAX_SIZE];
const char on_message[] = "ON\r";
const char off_message[] = "OFF\r";
const char heat_message[] = "HEAT\r";
const char cool_message[] = "COOL\r";
const char fault_message[] = "FAULT\r";
const char error_message[] = "ERROR\r";
const char ok_message[] = "OK\r";
const char usb_message[] = "USB\r";
const char one_amp_message[] = "1 AMP\r";
const char half_amp_message[] = "0.5 AMP\r";
int8_t received_setpoint_value;
int parameter;

/* Initialize com with ble */
void ble_init()
{
	sbi(BLE_DDR, RESETBLE_PIN);
	cbi(BLE_PORT, RESETBLE_PIN);
	_delay_ms(1000);
	sbi(BLE_PORT, RESETBLE_PIN);
	_delay_ms(1000);
	uartInit(BLE_BAUDRATE);
	_delay_ms(500);
	uartSendBuffer("AT+ROLE0");
	_delay_ms(500);
	uartSendBuffer("AT+RESET");
	_delay_ms(1500);
	uartSendBuffer("AT+NAMEYeCup");
	_delay_ms(500);
	clear_data_buffer();
}

/* copy command line*/
void copy_command ()
{
	// The USART might interrupt this - don't let that happen!
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		// Copy the contents of data_in into command_in
		for(int i = 0; i < COMMAND_MAX_SIZE; i++)
			command_in[i] = data_in[i];
		clear_data_buffer();
	}	
}


/*returns command and parameter if received*/
ble_command command_parser()
{
	if (get_command_ready() == FALSE)
		return YECUP_NO_COMMAND;
	copy_command();
	set_command_ready(FALSE);
	if(strstr(command_in, YECUP_GET_TEMPERATURE) != NULL)
		return YECUP_GET_TEMPERATURE_COMMAND;
	else if(strstr(command_in, YECUP_GET_BATTERY_STATE) != NULL)
		return YECUP_GET_BATTERY_STATE_COMMAND;
	else if(strstr(command_in, YECUP_GET_CHARGER_STATE) != NULL)
		return YECUP_GET_CHARGER_STATE_COMMAND;
	else if(strstr(command_in, YECUP_GET_FAN_STATE) != NULL)
		return YECUP_GET_FAN_STATE_COMMAND;
	else if(strstr(command_in, YECUP_GET_PELTIER_STATE) != NULL)
		return YECUP_GET_PELTIER_STATE_COMMAND;
	else if(strstr(command_in, YECUP_GET_CHARGE_CURRENT_STATE) != NULL)
		return YECUP_GET_CHARGE_CURRENT_STATE_COMMAND;
	else if(strstr(command_in, YECUP_GET_SETPOINT) != NULL)
		return YECUP_GET_SETPOINT_COMMAND;
	else if(strstr(command_in, YECUP_GET_FAN_POWER) != NULL)
		return YECUP_GET_FAN_POWER_COMMAND;
	else if(strstr(command_in, YECUP_GET_HEAT_POWER) != NULL)
		return YECUP_GET_HEAT_POWER_COMMAND;
	else if(strstr(command_in, YECUP_GET_COOL_POWER) != NULL)
		return YECUP_GET_COOL_POWER_COMMAND;
	else if(strstr(command_in, YECUP_TEST_BUTTONS) != NULL)
		return YECUP_TEST_BUTTONS_COMMAND;
	else if(strstr(command_in, YECUP_TEST_LED) != NULL)
		return YECUP_TEST_LED_COMMAND;
	else if(strstr(command_in, YECUP_TEST_PELTIER) != NULL)
		return YECUP_TEST_PELTIER_COMMAND;			
	else if(strstr(command_in, YECUP_SET_SETPOINT) != NULL)
	{
		char temp[4] = "";
		strncpy(temp, command_in + strlen(YECUP_SET_SETPOINT), 3);
		temp[4] = '\0';
		parameter = atoi(temp);		
		return YECUP_SET_SETPOINT_COMMAND;
	}
	else if(strstr(command_in, YECUP_SET_FAN_POWER) != NULL)
	{
		char temp[4] = "";
		strncpy(temp, command_in + strlen(YECUP_SET_FAN_POWER), 3);
		temp[4] = '\0';
		parameter = atoi(temp);
		return YECUP_SET_FAN_POWER_COMMAND;
	}
	else if(strstr(command_in, YECUP_SET_HEAT_POWER) != NULL)
	{
		char temp[4] = "";
		strncpy(temp, command_in + strlen(YECUP_SET_HEAT_POWER), 3);
		temp[4] = '\0';
		parameter = atoi(temp);
		return YECUP_SET_HEAT_POWER_COMMAND;
	}
	else if(strstr(command_in, YECUP_SET_COOL_POWER) != NULL)
	{
		char temp[4] = "";
		strncpy(temp, command_in + strlen(YECUP_SET_COOL_POWER), 3);
		temp[4] = '\0';
		parameter = atoi(temp);
		return YECUP_SET_COOL_POWER_COMMAND;
	}
	else
		return YECUP_NO_COMMAND;
}


/* change fan power */
void change_fan_power(int power)
{
	if((power < MIN_POWER) || (power > MAX_POWER))
	uartSendBuffer(error_message);
	else
	{
		set_fan_power((int8_t)power);
		uartSendBuffer(ok_message);
	}
}

/* change heat power */
void change_heat_power(int power)
{
	if((power < MIN_POWER) || (power > MAX_POWER))
	uartSendBuffer(error_message);
	else
	{
		set_peltier_heating_power((int8_t)power);
		uartSendBuffer(ok_message);
	}
}

/* change cool power */
void change_cool_power(int power)
{
	if((power < MIN_POWER) || (power > MAX_POWER))
	uartSendBuffer(error_message);
	else
	{
		set_peltier_cooling_power((int8_t)power);
		uartSendBuffer(ok_message);
	}
}

/* change setpoint temperature */
void change_setpoint_temperature(int temperature)
{
	if((temperature < MIN_SETPOINT_TEMP) || (temperature > MAX_SETPOINT_TEMP))
		uartSendBuffer(error_message);
	else
	{
		set_setpoint_temperature((int8_t)temperature);
		uartSendBuffer(ok_message);
	}
}


/* send setpoint temperature to ble*/
void send_setpoint_temperature()
{
	char buf[6];
	int8_t temperature = get_setpoint_temperature();
	itoa(temperature, buf, 10);
	uartSendBuffer(buf);
	uartSendByte('\r');
}

/* send fan power to ble*/
void send_fan_power()
{
	char buf[6];
	uint8_t power = get_fan_power();
	itoa(power, buf, 10);
	uartSendBuffer(buf);
	uartSendByte('\r');
}

/* send heat power to ble*/
void send_heat_power()
{
	char buf[6];
	uint8_t power = get_heat_power();
	itoa(power, buf, 10);
	uartSendBuffer(buf);
	uartSendByte('\r');
}

/* send cool power to ble*/
void send_cool_power()
{
	char buf[6];
	uint8_t power = get_cool_power();
	itoa(power, buf, 10);
	uartSendBuffer(buf);
	uartSendByte('\r');
}


/* send temperature to ble*/
void send_temperature()
{
	char buf[6];
	int16_t temperature = get_water_temperature();
	if(temperature == NTCTEMP_LOOKUPRETERROR)
		uartSendBuffer(error_message);
	else
	{
		itoa(temperature, buf, 10);
		uartSendBuffer(buf);
		uartSendByte('\r');
	}	
}


/* send battery state to ble*/
void send_battery_state()
{
	char buf[6];
	uint16_t bat_state = get_battery_state();
	if(bat_state == BATTERY_ERROR)
		uartSendBuffer(error_message);
	else
	{
		itoa(bat_state, buf, 10);
		uartSendBuffer(buf);
		uartSendByte('\r');
	}
}

/* send charger state to ble*/
void send_charger_state()
{
	if(get_charger_state() == CHARGER_OFF) 
		uartSendBuffer(off_message);
	else if(get_charger_state() == CHARGER_USB)
		uartSendBuffer(usb_message);	
	else if(get_charger_state() == CHARGER_ON)
		uartSendBuffer(on_message);
	else if(get_charger_state() == CHARGER_FAULT)
		uartSendBuffer(fault_message);
}

/* send fan state to ble*/
void send_fan_state()
{
	if(get_fan_state() == FAN_OFF)
		uartSendBuffer(off_message);
	else if(get_fan_state() == FAN_ON)
		uartSendBuffer(on_message);
	else if(get_fan_state() == FAN_FAULT)
		uartSendBuffer(fault_message);
}

/* send peltier state to ble*/
void send_peltier_state()
{
	if(get_peltier_state() == PELTIER_OFF)
		uartSendBuffer(off_message);
	else if(get_peltier_state() == PELTIER_HEATING)
		uartSendBuffer(heat_message);
	else if(get_peltier_state() == PELTIER_COOLING)
		uartSendBuffer(cool_message);
	else if(get_peltier_state() == PELTIER_FAULT)
		uartSendBuffer(fault_message);
}

/* send charge current state to ble*/
void send_charge_current_state()
{
	if(get_charge_current() == CHRG_CURR_HALF_AMP)
		uartSendBuffer(half_amp_message);
	else if(get_charge_current() == CHRG_CURR_ONE_AMP)
		uartSendBuffer(one_amp_message);
}

/* main function of command handling*/
void ble_command_handler()
{
	//int parameter = 0;	
	ble_command command = command_parser();
	switch(command)
	{
		case YECUP_GET_TEMPERATURE_COMMAND:
			send_temperature();
			break;
		case YECUP_GET_BATTERY_STATE_COMMAND:
			send_battery_state();
			break;
		case YECUP_GET_CHARGER_STATE_COMMAND:
			send_charger_state();
			break;
		case YECUP_GET_FAN_STATE_COMMAND:
			send_fan_state();
			break;
		case YECUP_GET_PELTIER_STATE_COMMAND:
			send_peltier_state();
			break;
		case YECUP_GET_CHARGE_CURRENT_STATE_COMMAND:
			send_charge_current_state();
			break;
		case YECUP_GET_SETPOINT_COMMAND:
			send_setpoint_temperature();
			break;
		case YECUP_GET_FAN_POWER_COMMAND:
			send_fan_power();
			break;
		case YECUP_GET_HEAT_POWER_COMMAND:
			send_heat_power();
			break;
		case YECUP_GET_COOL_POWER_COMMAND:
			send_cool_power();
			break;
		case YECUP_TEST_BUTTONS_COMMAND:
			button_test();
			break;
		case YECUP_TEST_LED_COMMAND:
			led_test();
			break;	
		case YECUP_TEST_PELTIER_COMMAND:
			peltier_test(DEFAULT_PELTIER_HEAT_POWER);
			break;	
		case YECUP_SET_SETPOINT_COMMAND:			
			change_setpoint_temperature(parameter);
			yecup_turn_on();
			break;
		case YECUP_SET_FAN_POWER_COMMAND:
			change_fan_power((uint8_t)parameter);
			break;
		case YECUP_SET_HEAT_POWER_COMMAND:
			change_heat_power((uint8_t)parameter);
			break;
		case YECUP_SET_COOL_POWER_COMMAND:
			change_cool_power((uint8_t)parameter);
			break;
		default:
			break;
	}
}


/* reset ble*/
void ble_reset()
{
	cbi(BLE_PORT, RESETBLE_PIN);
	_delay_ms(5);
	sbi(BLE_PORT, RESETBLE_PIN);
}