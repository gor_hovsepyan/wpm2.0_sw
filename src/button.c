/*
 * button.c
 *
 * Created: 04-Jul-16 10:49:28 AM
 *  Author: Hovsepyan Gor
 */ 


#include <avr/io.h>
#include "global.h"
#include <util/delay.h>
#include "button.h"
#include "led.h"

static button_state button_minus_state = BUTTON_IDLE;
static button_state button_plus_state = BUTTON_IDLE;

/* init buttons */
void button_init()
{
	BUTTON_PORT |= (1 << BUTTON_MINUS_PIN) | (1 << BUTTON_PLUS_PIN);
}

/* returns minus button state  */
uint8_t minus_but_state()
{
	static uint8_t state = 0;
	static uint16_t counter = 0;
	if (is_high(BUTTON_PIN, BUTTON_MINUS_PIN) && state)
	{
		while (counter++ < DEBOUNCE_TIMEOUT)
		{
			if(!is_high(BUTTON_PIN, BUTTON_MINUS_PIN))
				return state;
			_delay_us(100);
		}
		state = !state;
	}
	else if(!is_high(BUTTON_PIN, BUTTON_MINUS_PIN) && !state)
	{
		while (counter++ < DEBOUNCE_TIMEOUT)
		{
			if(is_high(BUTTON_PIN, BUTTON_MINUS_PIN))
				return state;
			_delay_us(100);
		}
		state = !state;
	}
	return state;
}

/* returns plus button state */
uint8_t plus_but_state()
{
	static uint8_t state = 0;
	uint16_t counter = 0;
	if (is_high(BUTTON_PIN, BUTTON_PLUS_PIN) && state)
	{
		while (counter++ < DEBOUNCE_TIMEOUT)
		{
			if(!is_high(BUTTON_PIN, BUTTON_PLUS_PIN))
				return state;
			_delay_us(100);
		}
		state = !state;
	}
	else if(!is_high(BUTTON_PIN, BUTTON_PLUS_PIN) && !state)
	{
		while (counter++ < DEBOUNCE_TIMEOUT)
		{
			if(is_high(BUTTON_PIN, BUTTON_PLUS_PIN))
				return state;
			_delay_us(100);
		}
		state = !state;
	}	
	return state;
}

/* handle buttons states */
void button_state_handler()
{
	static uint32_t minusheldCounter = 0;
	static uint32_t plusheldCounter = 0;
	if(minus_but_state() && get_minus_button_state() == BUTTON_IDLE)
	{
		if(minusheldCounter++ == HELD_TIMEOUT)
			set_minus_button_state(BUTTON_HELD);		
	}
	else if(!minus_but_state() && minusheldCounter < HELD_TIMEOUT && minusheldCounter > MIN_PRESS_TIMEOUT)
	{
		set_minus_button_state(BUTTON_PRESSED);
		minusheldCounter = 0;	
	}
	else if(!minus_but_state() && get_minus_button_state() == BUTTON_HELD)
	{
		set_minus_button_state(BUTTON_RELEASED);
		minusheldCounter = 0;
	}	
	else if(!minus_but_state())
		set_minus_button_state(BUTTON_IDLE);
	
	if(plus_but_state() && get_plus_button_state() == BUTTON_IDLE)
	{
		if(plusheldCounter++ == HELD_TIMEOUT)
		set_plus_button_state(BUTTON_HELD);
	}
	else if(!plus_but_state() && plusheldCounter < HELD_TIMEOUT && plusheldCounter > MIN_PRESS_TIMEOUT)
	{
		set_plus_button_state(BUTTON_PRESSED);
		plusheldCounter = 0;
	}
	else if(!plus_but_state() && get_plus_button_state() == BUTTON_HELD)
	{
		set_plus_button_state(BUTTON_RELEASED);
		plusheldCounter = 0;
	}	
	else if(!plus_but_state())
		set_plus_button_state(BUTTON_IDLE);
}

/* set minus button state */
void set_minus_button_state(button_state st)
{
	button_minus_state = st;
}

/* get minus button state */
button_state get_minus_button_state()
{
	return button_minus_state;
}


/* set plus button state */
void set_plus_button_state(button_state st)
{
	button_plus_state = st;
}

/* get plus button state */
button_state get_plus_button_state()
{
	return button_plus_state;
}




/* button test */
void button_test()
{
	uint32_t testCounter = 0;
	set_led_state(RED);
	while(testCounter++ < TEST_TIMEOUT)
	{
		_delay_ms(1);
		button_state_handler();
		if(((get_plus_button_state() == BUTTON_PRESSED) || (get_plus_button_state() == BUTTON_RELEASED))  && !minus_but_state())
		{
			if(get_led_state() == RED)
				set_led_state(GREEN);
			else if(get_led_state() == GREEN)
				set_led_state(BLUE);
			else if(get_led_state() == BLUE)
				set_led_state(RED);	
		}
		else if(((get_minus_button_state() == BUTTON_PRESSED) || (get_minus_button_state() == BUTTON_RELEASED)) && !plus_but_state())
		{
			if(get_led_state() == RED)
			set_led_state(BLUE);
			else if(get_led_state() == GREEN)
			set_led_state(RED);
			else if(get_led_state() == BLUE)
			set_led_state(GREEN);
		}
	}	
	set_led_state(OFF);
}