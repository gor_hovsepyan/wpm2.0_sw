/*
 * battery.c
 *
 * Created: 03.07.2016 14:26:30
 *  Author: Hovsepyan Gor
 */ 

#include <avr/io.h>
#include "global.h"
#include <util/delay.h>
#include "drivers/a2d.h"
#include "power.h"
#include <string.h>

uint16_t batteryState;
pwr_state chargeric_state = PASSIVE;
pwr_state  power_state = PASSIVE;

void power_init()
{
		DDRB |= (1<<DDRB1);     //Charger PinMode Output 
		DDRB |= (1<<DDRB2);    //Power PinMode output
		    
		DDRC &= ~(1<<DDRC0);
		PORTC=0x00;
}


void Enable_ChargerIC()
{
	PORTB |= (1 << CH_EN);
	chargeric_state = ACTIVE;
}

void Disable_ChargerIC()
{
	PORTB &= ~(1 << CH_EN);
	chargeric_state = PASSIVE;
}

void Enable_Power()
{
	PORTB |= (1 << POWER_EN);
	PORTD |= (1 << PD3);
	power_state = ACTIVE; 
}
void Disable_Power()
{
	PORTB &= ~(1 << POWER_EN);
	power_state = PASSIVE;
	PORTD &= ~(1 << PD3);
}

pwr_state Get_Charger_State()
{
	if(is_high(INPUT_PORT_DD, PD3))
	{
		return ACTIVE;  
	}
	else 
		return PASSIVE;
}
pwr_state Get_WC_State()
{
	if(is_high(INPUT_PORT_C, WC_ST_PIN))
	{
		return ACTIVE;
	}
	else
	return PASSIVE;
}
pwr_state Get_ChargerIC_State()
{
	return chargeric_state; 
}

pwr_state Get_Power_State()
{
	return power_state;
} 
uint16_t Calculate_battery_level(uint16_t adc_value)
{
	
	uint16_t batState = 0;
	if(adc_value == 0)
	return BATTERY_ERROR;
	if(adc_value < ADC_MIN_VOLTAGE)
	return 0;
	batState = ((uint32_t)(adc_value - ADC_MIN_VOLTAGE) * 100) / (ADC_MAX_VOLTAGE - ADC_MIN_VOLTAGE);
	if(batState > 100)
	batState = 100;
	return batState;
	
}

uint16_t battery_state_handler() 
{
	static uint16_t adc_sum_value = 0; 
	static uint16_t window_counter = 0;
	uint16_t adc_value;
	adc_value = a2dConvert10bit(BATT_ADC_CH);
	adc_sum_value += adc_value;
	if(++window_counter == BATT_WINDOW_SAMPLES)
	{
		adc_value =adc_sum_value / window_counter;
		window_counter = adc_sum_value = 0;
		return adc_value;
	}
}

uint16_t get_battery_state()
{
	return a2dConvert10bit(BATT_ADC_CH)/*Calculate_battery_level(a2dConvert10bit(BATT_ADC_CH))*/; 
}











