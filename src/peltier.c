/*
 * peltier.c
 *
 * Created: 03.07.2016 18:36:16
 *  Author: Van Petrosyan
 */ 

#include <avr/io.h>
#include "global.h"
#include <util/delay.h>
#include "drivers/a2d.h"
#include "led.h"
#include "peltier.h"

static fan_state fanState = FAN_OFF;
static peltier_state peltierState = PELTIER_OFF;
static uint8_t fanPower;
static uint8_t heatPower;
static uint8_t coolPower;

/* Peltier element initialization*/
void peltier_init()
{
	FAN_DDR |= 1 << FAN_PIN;
	PELTIER_DDR |= (1 << PELTIER_COOL_PIN) | (1 << PELTIER_HEAT_PIN);
	sbi(DIDR0, ADC4D);
	TCCR0A = (1 << COM0A1) | (1 << WGM01) | (1 << WGM00); /* Fast PWM 8 bit for fan */
	TCCR1A = (1 << WGM10);  /* Fast PWM 8 bit for peltier 2 channel */ 
	TCCR1B = (1 << WGM12);
	set_fan_power(DEFAULT_FAN_POWER);
	set_peltier_heating_power(DEFAULT_PELTIER_HEAT_POWER);
	set_peltier_cooling_power(DEFAULT_PELTIER_COOL_POWER);
}

/* get fan power */
uint8_t get_fan_power()
{
	return fanPower;
}

/* get heat power */
uint8_t get_heat_power()
{
	return heatPower;
}

/* get cool power */
uint8_t get_cool_power()
{
	return coolPower;
}


/* set power in percents for fan pwm */
void set_fan_power(uint8_t power)
{
	fanPower = power;
	OCR0A = ((uint16_t)power * 255) / 100;
}

/* set heating power in percents for peltier pwm */
void set_peltier_heating_power(uint8_t power)
{
	heatPower = power;
	OCR1B = ((uint16_t)power * 255) / 100;
}

/* set cooling power in percents for peltier pwm */
void set_peltier_cooling_power(uint8_t power)
{
	coolPower = power;
	OCR1A = ((uint16_t)power * 255) / 100;
}

/* set fan state */
void set_fan_state(fan_state st)
{
	if(fanState != st)
	{
		fanState = st;
		switch(st)
		{
			case FAN_OFF:
				TCCR0B &= ~DEFAULT_FAN_PWM_CLK;
				TCCR0A &= ~(1 << COM0A1);
				TCNT0 = 0;
				cbi(FAN_PORT, FAN_PIN);
				break;
			case FAN_ON:
				TCCR0A |= 1 << COM0A1;
				TCCR0B |= DEFAULT_FAN_PWM_CLK;
				break;
			default:
				break;
		}
	}
}


/* set peltier state */
void set_peltier_state(peltier_state st)
{
	if(peltierState != st)
	{
		peltierState = st;
		switch(st)
		{
			case PELTIER_OFF:
				TCCR1B &= ~DEFAULT_PELTIER_PWM_CLK;
				TCNT1 = TCCR1A = 0;
				cbi(PELTIER_PORT, PELTIER_COOL_PIN);
				cbi(PELTIER_PORT, PELTIER_HEAT_PIN);
				break;
			case PELTIER_HEATING:
				TCCR1B &= ~DEFAULT_PELTIER_PWM_CLK;
				TCNT1 = 0;
				cbi(PELTIER_PORT, PELTIER_COOL_PIN);
				cbi(PELTIER_PORT, PELTIER_HEAT_PIN);
				TCCR1A = (1 << COM1B1) | (1 << WGM10);
				TCCR1B |= DEFAULT_PELTIER_PWM_CLK;
				break;
			case PELTIER_COOLING:
				TCCR1B &= ~DEFAULT_PELTIER_PWM_CLK;
				TCNT1 = 0;
				cbi(PELTIER_PORT, PELTIER_COOL_PIN);
				cbi(PELTIER_PORT, PELTIER_HEAT_PIN); 
				TCCR1A = (1 << COM1A1) | (1 << WGM10);
				TCCR1B |= DEFAULT_PELTIER_PWM_CLK;
				break;
			default:
				break;
		}
	}
}

/* return fan state */
fan_state get_fan_state()
{
	return fanState;
}


/* return peltier state */
peltier_state get_peltier_state()
{
	return peltierState;
}

/* flash red led if peltier not working */
void peltier_adc_test()
{
	uint16_t adc_value = 0;
	adc_value = a2dConvert10bit(PLTR_ADC_CH);
	if(adc_value < MIN_PELTIER_VOLTAGE)
	{
		set_peltier_state(PELTIER_FAULT);
		flash_led(RED);
	}
	else
		flash_led(GREEN);
}

/*test for peltier*/
void peltier_test(uint8_t power)
{
	
	set_peltier_heating_power(power);
	set_peltier_cooling_power(power);
	set_peltier_state(PELTIER_OFF);
	_delay_ms(1000);
	set_peltier_state(PELTIER_HEATING);
	_delay_ms(1000);
	peltier_adc_test();	
	set_peltier_state(PELTIER_COOLING);
	_delay_ms(1000);
	peltier_adc_test();
	set_peltier_state(PELTIER_OFF);
}

/*test for fan*/
void fan_test(uint8_t power)
{
	set_fan_power(power);
	set_fan_state(FAN_OFF);
	_delay_ms(1000);
	set_fan_state(FAN_ON);
	_delay_ms(1000);
}